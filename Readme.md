# Advent of Code 2020

> Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language you like.

These are my solutions for [calendar year 2020](https://adventofcode.com/2020), made in C++.

## Requirements

CMake and a C++17 capable compiler

## Build / run instructions:
```sh
mkdir build
cd build
cmake ..
make

./day01
./day02
# etc
```