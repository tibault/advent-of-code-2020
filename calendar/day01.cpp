#include <iostream>
#include <fstream>
#include <vector>

void puzzle()
{
    std::fstream input;
    input.open("../input/input-01.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");
    std::vector<int64_t> values;
    while (!input.fail()) {
        int64_t v;
        input >> v;
        values.push_back(v);
    }
    for (auto a : values) {
        for (auto b : values) {
            if (a + b == 2020) {
                std::cout << a << " * " << b << " = " << a*b << "\n";
            }
        }
    }
    for (auto a : values) {
        for (auto b : values) {
            for (auto c : values) {
                if (a + b + c == 2020) {
                    std::cout << a << " * " << b << " * " << c << " = " << a*b*c << "\n";
                }
            }
        }
    }
}

int main()
{
    try {
        puzzle();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}