#include <iostream>
#include <fstream>
#include <regex>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-02.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    const std::regex policy("([0-9]+)-([0-9]+) (.): (.+)");
    std::smatch matches;

    int64_t correct = 0;
    int64_t total = 0;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (std::regex_match(line, matches, policy)) {
            ++total;

            int min = std::stoi(matches[1].str());
            int max = std::stoi(matches[2].str());
            char c = matches[3].str()[0];

            auto pwd = matches[4].str();
            int count = 0;
            for (char x : pwd) {
                if (x == c)
                    ++count;
            }
            if (count >= min && count <= max)
                ++correct;
        }
    }

    std::cout << correct << " of " << total << " are correct\n";
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-02.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    const std::regex policy("([0-9]+)-([0-9]+) (.): (.+)");
    std::smatch matches;

    int64_t correct = 0;
    int64_t total = 0;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (std::regex_match(line, matches, policy)) {
            ++total;

            int a = std::stoi(matches[1].str()) - 1;
            int b = std::stoi(matches[2].str()) - 1;
            char c = matches[3].str()[0];

            auto pwd = matches[4].str();
            if (pwd[a] == c || pwd[b] == c)
                if (pwd[a] != pwd[b])
                    ++correct;
        }
    }

    std::cout << correct << " of " << total << " are correct\n";
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}