#include <iostream>
#include <fstream>

int64_t puzzle_a(int right = 3, int down = 1)
{
    std::fstream input;
    input.open("../input/input-03.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");
    int x = 0;
    int64_t trees = 0;
    std::string line;
    std::getline(input, line);
    while (!input.fail()) {
        if (line.size() > 0) {
            if (line[x] == '#') {
                ++trees;
            }
            x = (x + right) % line.length();

            for (int i = 0; i < down; ++i)
                std::getline(input, line);
        }
    }
    std::cout << trees << std::endl;
    return trees;
}

void puzzle_b()
{
    std::cout
       << puzzle_a(1, 1)
        * puzzle_a(3, 1)
        * puzzle_a(5, 1)
        * puzzle_a(7, 1)
        * puzzle_a(1, 2)
    << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}