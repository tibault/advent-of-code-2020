#include <iostream>
#include <fstream>
#include <regex>
#include <set>
#include <sstream>

void puzzle_a()
{
    // byr (Birth Year)
    // iyr (Issue Year)
    // eyr (Expiration Year)
    // hgt (Height)
    // hcl (Hair Color)
    // ecl (Eye Color)
    // pid (Passport ID)
    // cid (Country ID)    [ignore this one]

    std::fstream input;
    input.open("../input/input-04.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::set<std::string> keys;
    int64_t valid = 0;

    std::string line;
    //std::getline(input, line);
    while (!input.fail()) {
        std::getline(input, line);
        if (line.empty()) {
            // test keys
            if (keys.size() == 7) {
                ++valid;
            }
            keys.clear();
        } else {
            // add keys
            if (line.find("byr:") != std::string::npos)
                keys.insert("byr");
            if (line.find("iyr:") != std::string::npos)
                keys.insert("iyr");
            if (line.find("eyr:") != std::string::npos)
                keys.insert("eyr");
            if (line.find("hgt:") != std::string::npos)
                keys.insert("hgt");
            if (line.find("hcl:") != std::string::npos)
                keys.insert("hcl");
            if (line.find("ecl:") != std::string::npos)
                keys.insert("ecl");
            if (line.find("pid:") != std::string::npos)
                keys.insert("pid");
            // if (line.find("cid:") != std::string::npos)
            //     keys.insert("cid");
        }

    }
    std::cout << valid << std::endl;
}

void puzzle_b()
{
    // byr (Birth Year)
    // iyr (Issue Year)
    // eyr (Expiration Year)
    // hgt (Height)
    // hcl (Hair Color)
    // ecl (Eye Color)
    // pid (Passport ID)
    // cid (Country ID)    [ignore this one]

    std::fstream input;
    input.open("../input/input-04.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::set<std::string> keys;
    int64_t valid = 0;

    std::smatch matches;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (line.empty()) {
            // test keys
            if (keys.size() == 7) {
                ++valid;
            }
            keys.clear();
        } else {
            // add keys
            std::stringstream sline(line);
            std::string field;
            while (getline(sline, field, ' ')) {
                if (field.size() > 4) {
// byr (Birth Year) - four digits; at least 1920 and at most 2002.
                    static const std::regex byr("byr:([0-9]{4})");
                    if (std::regex_match(field, matches, byr)) {
                        int y = std::stoi(matches[1].str());
                        if (y >= 1920 && y <= 2002)
                            keys.insert("byr");
                    }
// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
                    static const std::regex iyr("iyr:([0-9]{4})");
                    if (std::regex_match(field, matches, iyr)) {
                        int y = std::stoi(matches[1].str());
                        if (y >= 2010 && y <= 2020)
                            keys.insert("iyr");
                    }
// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
                    static const std::regex eyr("eyr:([0-9]{4})");
                    if (std::regex_match(field, matches, eyr)) {
                        int y = std::stoi(matches[1].str());
                        if (y >= 2020 && y <= 2030)
                            keys.insert("eyr");
                    }
// hgt (Height) - a number followed by either cm or in:
//     If cm, the number must be at least 150 and at most 193.
//     If in, the number must be at least 59 and at most 76.
                    static const std::regex hgt("hgt:([0-9]+)(cm|in)");
                    if (std::regex_match(field, matches, hgt)) {
                        int x = std::stoi(matches[1].str());
                        auto u = matches[2].str();
                        if (u == "cm") {
                            if (x >= 150 && x <= 193)
                                keys.insert("hgt");
                        } else if (u == "in") {
                            if (x >= 59 && x <= 76)
                                keys.insert("hgt");
                        }
                    }
// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
                    static const std::regex hcl("hcl:#([0-9a-f]{6})");
                    if (std::regex_match(field, matches, hcl)) {
                        keys.insert("hcl");
                    }
// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
                    static const std::regex ecl("ecl:(amb|blu|brn|gry|grn|hzl|oth)");
                    if (std::regex_match(field, matches, ecl)) {
                        keys.insert("ecl");
                    }
// pid (Passport ID) - a nine-digit number, including leading zeroes.
                    static const std::regex pid("pid:([0-9]{9})");
                    if (std::regex_match(field, matches, pid)) {
                        keys.insert("pid");
                    }
                }
            }
        }
    }
    std::cout << valid << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}