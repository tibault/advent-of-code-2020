#include <iostream>
#include <fstream>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-05.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    int max_row = 0;
    int max_col = 0;
    int max_seat = 0;

    std::string line;
    std::getline(input, line);
    while (!input.fail()) {
        if (line.empty())
            break;
        int row = 0;
        int col = 0;
        for (int i = 0; i < 7; i++) {
            row += (1 << (6-i))*(line[i] == 'B' ? 1 : 0);
        }
        for (int i = 0; i < 3; i++) {
            col += (1 << (2-i))*(line[7+i] == 'R' ? 1 : 0);
        }
        if (row > max_row)
            max_row = row;
        if (col > max_col)
            max_col = col;
        int seat = (row*8)+col;
        if (seat > max_seat)
            max_seat = seat;
        std::getline(input, line);
    }
    std::cout << max_seat << std::endl;
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-05.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    bool ids[1024]{false};
    bool seats[128][8]{false};

    std::string line;
    std::getline(input, line);
    while (!input.fail()) {
        if (line.empty())
            break;
        int row = 0;
        int col = 0;
        for (int i = 0; i < 7; i++) {
            row += (1 << (6-i))*(line[i] == 'B' ? 1 : 0);
        }
        for (int i = 0; i < 3; i++) {
            col += (1 << (2-i))*(line[7+i] == 'R' ? 1 : 0);
        }
        int seat = (row*8)+col;

        ids[seat] = true;
        seats[row][col] = true;

        std::getline(input, line);
    }
    bool found_start = false;
    for (int r = 0; r < 128; r++) {
        for (int c = 0; c < 8; c++) {
            if (found_start) {
                if (!seats[r][c]) {
                     std::cout << r << " "
                               << c << " : "
                               << (r*8)+c
                               << std::endl;
                     return;
                }
            } else {
                if (seats[r][c]) {
                    found_start = true;
                }
            }
        }
    }
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}