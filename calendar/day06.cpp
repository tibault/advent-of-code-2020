#include <iostream>
#include <fstream>
#include <set>
#include <map>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-06.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::set<char> grp;
    int sum = 0;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (line.empty()) {
            sum += grp.size();
            grp.clear();
        } else {
            for (char c : line) {
                grp.insert(c);
            }
        }
    }
    std::cout << sum << std::endl;
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-06.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::map<char, int> grp;
    int grp_size = 0;
    int sum = 0;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (line.empty()) {
            for (auto [ans, cnt] : grp) {
                if (cnt == grp_size) {
                    ++sum;
                }
            }
            grp.clear();
            grp_size = 0;
        } else {
            ++grp_size;
            for (char c : line) {
                grp[c]++;
            }
        }
    }
    std::cout << sum << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}