#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>
#include <queue>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-07.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    const std::regex start_rule("([a-z ]+) bags contain .*");
    const std::regex inside_rule("([0-9]+|no) ([a-z ]+) bags?");

    std::multimap<std::string, std::string> found_in;

    std::string line;
    std::smatch matches;
    while (!input.fail()) {
        std::getline(input, line);

        if (std::regex_match(line, matches, start_rule)) {
            auto target = matches[1].str();
            while (std::regex_search(line, matches, inside_rule)) {
                if (matches[1].str() != "no") {
                    found_in.insert({ matches[2].str(), target });
                }
                line = matches.suffix();
            }
        }
    }

    std::set<std::string> r;
    std::map<std::string, bool> seen;

    std::queue<std::string> todo;
    todo.push("shiny gold");

    while (!todo.empty()) {
        auto range = found_in.equal_range(todo.front());
        for (auto i = range.first; i != range.second; i++) {
            if (!seen[i->second])
                todo.push(i->second);
        }
        seen[todo.front()] = true;
        todo.pop();
    }
    std::cout << seen.size() - 1 << " colours" << std::endl;
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-07.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    const std::regex start_rule("([a-z ]+) bags contain .*");
    const std::regex inside_rule("([0-9]+|no) ([a-z ]+) bags?");

    std::multimap<std::string, std::string> found_in;
    struct content {
        int count{0};
        std::string colour;
    };
    std::multimap<std::string, content> contents;
    std::map<std::string, int64_t> bags; // the bag and its contents
    std::queue<std::string> todo;

    std::string line;
    std::smatch matches;
    while (!input.fail()) {
        std::getline(input, line);

        if (std::regex_match(line, matches, start_rule)) {
            auto target = matches[1].str();
            while (std::regex_search(line, matches, inside_rule)) {
                if (matches[1].str() != "no") {
                    found_in.insert({ matches[2].str(), target });
                    contents.insert({
                        target,
                        {
                            std::stoi(matches[1].str()),
                            matches[2].str()
                        }
                    });
                } else {
                    bags[target] = 1; // the bag + its 0 contents
                    todo.push(target);
                }
                line = matches.suffix();
            }
        }
    }

    // returns -1 if we don't know all child bags yet
    auto get_bag_count = [&](const std::string& key) -> int64_t {
        int64_t count = 0;
        auto range = contents.equal_range(key);
        for (auto i = range.first; i != range.second; i++) {
            if (auto b = bags.find(i->second.colour); b != bags.end()) {
                count += (i->second.count * b->second);
            } else {
                return -1;
            }
        }
        return count;
    };

    // todo contains the ones we know, work your way up
    while (!todo.empty()) {
        auto range = found_in.equal_range(todo.front());
        for (auto i = range.first; i != range.second; i++) {
            if (auto c = get_bag_count(i->second); c >= 0) {
                bags[i->second] = c + 1;
                todo.push(i->second);
            }
        }
        todo.pop();
    }

    std::cout << bags["shiny gold"] - 1 << " bags" << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();
    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}