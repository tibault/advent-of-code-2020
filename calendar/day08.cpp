#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-08.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    struct Instruction {
        std::string cmd;
        int arg{0};
        bool executed{false};
    };
    std::vector<Instruction> program;

    const std::regex regex_instr("(...) ([+-][0-9]+)");
    std::smatch matches;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);

        if (std::regex_match(line, matches, regex_instr)) {
            program.push_back({
                matches[1].str(),
                std::stoi(matches[2].str()),
                false
            });
        }
    }
    int acc = 0;
    size_t ptr = 0;
    while (ptr < program.size() && !program[ptr].executed) {
        program[ptr].executed = true;
        if (program[ptr].cmd == "jmp") {
            ptr += program[ptr].arg;
        } else if (program[ptr].cmd == "acc") {
            acc += program[ptr].arg;
            ++ptr;
        } else { // nop
            ++ptr;
        }
    }
    std::cout << "acc: " << acc << std::endl;
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-08.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    struct Instruction {
        std::string cmd;
        int arg{0};
        bool executed{false};
    };
    std::vector<Instruction> program;

    const std::regex regex_instr("(...) ([+-][0-9]+)");
    std::smatch matches;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);

        if (std::regex_match(line, matches, regex_instr)) {
            program.push_back({
                matches[1].str(),
                std::stoi(matches[2].str()),
                false
            });
        }
    }

    auto test_program = [](std::vector<Instruction>& program) {
        int acc = 0;
        size_t ptr = 0;
        while (ptr < program.size()) {
            if (program[ptr].executed)
                throw false;
            program[ptr].executed = true;
            if (program[ptr].cmd == "jmp") {
                ptr += program[ptr].arg;
            } else if (program[ptr].cmd == "acc") {
                acc += program[ptr].arg;
                ++ptr;
            } else { // nop
                ++ptr;
            }
        }
        return acc;
    };

    for (size_t ptr = 0; ptr < program.size(); ++ptr) {
        if (program[ptr].cmd != "acc") {
            auto fixed_program = program;
            if (program[ptr].cmd == "jmp") {
                fixed_program[ptr].cmd = "nop";
            } else if (program[ptr].cmd == "nop") {
                fixed_program[ptr].cmd = "jmp";
            } else {
                continue; // should never reach this but whatever
            }
            try {
                auto result = test_program(fixed_program);
                std::cout << "result: " << result << std::endl;
                return;
            } catch (...) {}
        }
    }
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}