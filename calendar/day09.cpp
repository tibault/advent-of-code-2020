#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>
#include <deque>

int puzzle_a()
{
    std::fstream input;
    input.open("../input/input-09.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    const size_t preamble_size = 25;

    std::vector<int> window;
    std::vector<int> sums; // sum up to yourself, then skip to next number

    // sums will look like this for window [a,b,c,d,e,f]:
    // b+a, c+a, c+b, d+a, d+b, d+c, e+a, e+b, e+c, e+d, f+a, ...
    // 0  , 1  , 2  , 3  , 4  , 5  , 6  , 7  , 8  , 9  , 10 , ...
    // x    x         x              x                   x
    // 0    +1        +2             +3                  +4
    // -> all sums with the oldest number can be found at predictable locations!
    // -> remove those indexes + add the new sums at the end

    std::string line;
    // load preamble
    for (size_t p = 0; !input.fail() && p < preamble_size; ++p) {
        std::getline(input, line);
        if (!line.empty()) {
            window.push_back(std::stoi(line)); // don't care about exceptions
            for (size_t i = 0; i < p; ++i) {
                sums.push_back(window[p] + window[i]);
            }
        }
    }

    // start walking
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            int x = std::stoi(line); // don't care about exceptions

            auto it = std::find(sums.begin(), sums.end(), x);
            if (it == sums.end())
                return x; // game over!

            window.erase(window.begin());
            // move the sums we want to keep forward, to make room for the new sums
            size_t i_new = 0;
            for (size_t i_old = 2, chunk = 1; chunk < preamble_size - 1; ++i_old) {
                for (size_t s = 0; s < chunk; ++s) {
                    sums[i_new++] = sums[i_old++];
                }
                ++chunk;
            }
            // fill in the new sums
            for (size_t i = 0; i < window.size(); ++i) {
                sums[i_new++] = x + window[i];
            }
            window.push_back(x);
        }
    }
    return -1;
}

int puzzle_b(int fault)
{
    std::fstream input;
    input.open("../input/input-09.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");
    std::deque<int> window;
    int sum = 0;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            int x = std::stoi(line); // don't care about exceptions
            window.push_back(x);
            sum += x;

            while (sum > fault) {
                sum -= window.front();
                window.pop_front();
            }

            if (sum == fault) {
                std::sort(window.begin(), window.end());
                return window.front() + window.back();
            }
        }
    }
    return 0;
}

int main()
{
    try {
        int fault = puzzle_a();
        std::cout << "Found bad number: " << fault << "\n";
        std::cout << "Found weakness: " << puzzle_b(fault) << std::endl;
    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}