#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-10.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::vector<int> jolts;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            jolts.push_back(std::stoi(line));
        }
    }
    std::sort(jolts.begin(), jolts.end());
    jolts.push_back(jolts[jolts.size() - 1] + 3);

    std::map<int, int> diff_count;
    diff_count[jolts[0]]++;
    for (size_t i = 1; i < jolts.size(); ++i) {
        diff_count[jolts[i] - jolts[i-1]]++;
    }
    std::cout << diff_count[1]*diff_count[3] << std::endl;
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-10.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::vector<int> jolts;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            jolts.push_back(std::stoi(line));
        }
    }
    std::sort(jolts.begin(), jolts.end());
    int last = jolts[jolts.size() - 1] + 3;
    jolts.push_back(last);
    jolts.insert(jolts.begin(), 0);

    std::map<int, int64_t> solution;
    solution[last] = 1;
    // solve for 0 by working backwards
    for (size_t j = jolts.size() - 1; j > 0; --j) {
        int jolt = jolts[j-1];
        int64_t j_sol = 0;
        // valid jumps: [current + 1, current + 3]
        for (int diff = 1; diff <= 3; ++diff) {
            if (auto it = solution.find(jolt + diff); it != solution.end()) {
                j_sol += it->second;
            }
        }
        if (j_sol) {
            solution[jolt] = j_sol;
        }
    }
    std::cout << solution[0] << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}