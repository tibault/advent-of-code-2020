#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

enum seat_state {
    ss_floor = 0,
    ss_empty,
    ss_occupied,
};

// I may have overcomplicated this one a bit because I didn't
// want to write std::vector<std::vector<seat_state>>   ~_~"
void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-11.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    size_t col = 0;
    std::vector<seat_state> seats; // use %col for rows

    std::string line;
    std::getline(input, line);
    col = line.size();
    do {
        for (char c : line) {
            if (c == 'L') {
                seats.push_back(ss_empty);
            } else if (c == '#') {
                seats.push_back(ss_occupied);
            } else if (c == '.') {
                seats.push_back(ss_floor);
            }
        }
        std::getline(input, line);
    } while (!input.fail());

    //  0  1  2  3  4
    //  5  6  7  8  9
    // 10 11 12 13 14
    //
    // col size cs: 5
    // for p 7:
    // L =  6 (p-1)   ,  R =  8 (p+1)
    // T =  2 (p-cs)  ,  B = 12 (p+cs)
    // TL=  1 (p-cs-1), TR =  3 (p-cs+1)
    // BL= 11 (p+cs-1), BR = 13 (p+cs+1)
    //
    // and check modulo cs for edges
    //
    auto adj_seats = [](size_t p, size_t cs,
                        const std::vector<seat_state>& v) {
        int res = 0;
        bool t = p >= cs;
        bool b = v.size()-p > cs;
        bool l = p%cs;
        bool r = (p+1)%cs;

        if (t) {
            if (l && v[p-cs-1] == ss_occupied) ++res;
            if (     v[p-cs  ] == ss_occupied) ++res;
            if (r && v[p-cs+1] == ss_occupied) ++res;
        }
        {
            if (l && v[p-1] == ss_occupied) ++res;
            if (r && v[p+1] == ss_occupied) ++res;
        }
        if (b) {
            if (l && v[p+cs-1] == ss_occupied) ++res;
            if (     v[p+cs  ] == ss_occupied) ++res;
            if (r && v[p+cs+1] == ss_occupied) ++res;
        }
        return res;
    };

    auto get_next = [&adj_seats](size_t p, size_t cs,
                                 const std::vector<seat_state>& v) {
        // If a seat is empty (L) and there are no occupied seats adjacent
        // to it, the seat becomes occupied.
        // If a seat is occupied (#) and four or more seats adjacent to it
        // are also occupied, the seat becomes empty.
        // Otherwise, the seat's state does not change.
        switch (v[p]) {
        case ss_empty:
            return (adj_seats(p, cs, v) > 0) ? ss_empty : ss_occupied;
        case ss_occupied:
            return (adj_seats(p, cs, v) >= 4) ? ss_empty : ss_occupied;
        default:
            return ss_floor;
        }
    };

    // init done, let's run the algo!
    bool still_moving = true;
    while (still_moving) {
        std::vector<seat_state> next(seats.size());
        bool change = false;
        for (size_t p = 0; p < seats.size(); ++p) {
            auto n = get_next(p, col, seats);
            if (seats[p] != n) {
                change = true;
            }
            next[p] = n;
        }
        if (change) {
            seats = next;
        } else {
            still_moving = false;
        }
    }
    auto occ = std::count_if(seats.begin(), seats.end(), [](auto ss){
        return ss == ss_occupied;
    });

    std::cout << "Settled on " << occ << " occupied seats" << std::endl;
}

// I'll go ahead and use a double vector this time :p
void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-11.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::vector<std::vector<seat_state>> seats;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            std::vector<seat_state> row;
            for (char c : line) {
                if (c == 'L') {
                    row.push_back(ss_empty);
                } else if (c == '#') {
                    row.push_back(ss_occupied);
                } else if (c == '.') {
                    row.push_back(ss_floor);
                }
            }
            seats.push_back(row);
        }
    }

    // 'adjacent' now also means look past ss_floor
    auto adj_seats = [&](int row, int col) {
        int adj = 0;
        // top left
        for (int r = row-1, c = col-1; r >= 0 && c >= 0; --r, --c) {
            if (seats[r][c] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[r][c] == ss_empty) {
                break;
            }
        }
        // top
        for (int r = row-1; r >= 0; --r) {
            if (seats[r][col] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[r][col] == ss_empty) {
                break;
            }
        }
        // top right
        for (int r = row-1, c = col+1; r >= 0 && c < seats[r].size(); --r, ++c) {
            if (seats[r][c] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[r][c] == ss_empty) {
                break;
            }
        }

        // left
        for (int c = col-1; c >= 0; --c) {
            if (seats[row][c] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[row][c] == ss_empty) {
                break;
            }
        }
        // right
        for (int c = col+1; c < seats[row].size(); ++c) {
            if (seats[row][c] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[row][c] == ss_empty) {
                break;
            }
        }

        // bottom left
        for (int r = row+1, c = col-1; r < seats.size() && c >= 0; ++r, --c) {
            if (seats[r][c] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[r][c] == ss_empty) {
                break;
            }
        }
        // bottom
        for (int r = row+1; r < seats.size(); ++r) {
            if (seats[r][col] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[r][col] == ss_empty) {
                break;
            }
        }
        // bottom right
        for (int r = row+1, c = col+1; r < seats.size() && c < seats[r].size(); ++r, ++c) {
            if (seats[r][c] == ss_occupied) {
                ++adj;
                break;
            } else if (seats[r][c] == ss_empty) {
                break;
            }
        }
        return adj;
    };


    auto get_next = [&](int r, int c) {
        // If a seat is empty (L) and there are no occupied seats adjacent
        // to it, the seat becomes occupied.
        // If a seat is occupied (#) and five or more seats adjacent to it
        // are also occupied, the seat becomes empty.
        // Otherwise, the seat's state does not change.
        switch (seats[r][c]) {
        case ss_empty:
            return (adj_seats(r, c) > 0) ? ss_empty : ss_occupied;
        case ss_occupied:
            return (adj_seats(r, c) >= 5) ? ss_empty : ss_occupied;
        default:
            return ss_floor;
        }
    };

    // init done, let's run the algo!
    bool still_moving = true;
    size_t occ = 0;
    while (still_moving) {
        std::vector<std::vector<seat_state>> next(seats.size());
        bool change = false;
        for (size_t r = 0; r < seats.size(); ++r) {
            next[r].resize(seats[r].size());
            for (size_t c = 0; c < seats[r].size(); ++c) {
                auto n = get_next(r, c);
                if (n == ss_occupied)
                    ++occ;
                if (seats[r][c] != n)
                    change = true;
                next[r][c] = n;
            }
        }
        if (change) {
            seats = next;
            occ = 0;
        } else {
            still_moving = false;
        }
    }

    std::cout << "Settled on " << occ << " occupied seats" << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}