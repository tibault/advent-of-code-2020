#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

void puzzle_a()
{
    // Action N means to move north by the given value.
    // Action S means to move south by the given value.
    // Action E means to move east by the given value.
    // Action W means to move west by the given value.
    // Action L means to turn left the given number of degrees.  (multiple of 90)
    // Action R means to turn right the given number of degrees. (multiple of 90)
    // Action F means to move forward by the given value in the direction the ship is currently facing.
    std::fstream input;
    input.open("../input/input-12.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    int64_t x = 0, y = 0;
    int direction = 90;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            char c = line[0];
            int i = std::stoi(line.substr(1));
            if (c == 'N') {
                y += i;
            } else if (c == 'S') {
                y -= i;
            } else if (c == 'E') {
                x += i;
            } else if (c == 'W') {
                x -= i;
            } else if (c == 'L') {
                direction -= i;
            } else if (c == 'R') {
                direction += i;
            } else if (c == 'F') {
                direction %= 360;
                switch(direction) {
                case 0:
                    y += i;
                    break;
                case 90:
                case -270:
                    x += i;
                    break;
                case 180:
                case -180:
                    y -= i;
                    break;
                case 270:
                case -90:
                    x -= i;
                    break;
                }
            }
        }
    }

    std::cout << std::abs(x) + std::abs(y) << std::endl;
}

void puzzle_b()
{
    // Action N means to move the waypoint north by the given value.
    // Action S means to move the waypoint south by the given value.
    // Action E means to move the waypoint east by the given value.
    // Action W means to move the waypoint west by the given value.
    // Action L means to rotate the waypoint around the ship left (counter-clockwise) the given number of degrees.
    // Action R means to rotate the waypoint around the ship right (clockwise) the given number of degrees.
    // Action F means to move forward to the waypoint a number of times equal to the given value.
    std::fstream input;
    input.open("../input/input-12.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    int64_t x = 0, y = 0;
    int64_t dx = 10, dy = 1;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (!line.empty()) {
            char c = line[0];
            int i = std::stoi(line.substr(1));
            if (c == 'N') {
                dy += i;
            } else if (c == 'S') {
                dy -= i;
            } else if (c == 'E') {
                dx += i;
            } else if (c == 'W') {
                dx -= i;
            } else if (c == 'L' || c == 'R') {
                if (c == 'L') {
                    i = (360-i)%360;
                }
                switch(i) {
                case 90:
                    std::swap(dx, dy);
                    dy *= -1;
                    break;
                case 180:
                    dx *= -1;
                    dy *= -1;
                    break;
                case 270:
                    std::swap(dx, dy);
                    dx *= -1;
                    break;
                }
            } else if (c == 'F') {
                x += i * dx;
                y += i * dy;
            }
        }
    }

    std::cout << std::abs(x) + std::abs(y) << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}