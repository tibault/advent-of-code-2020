#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-13.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    int64_t arrival = 0;
    int64_t bus = 0;
    int64_t wait = INT64_MAX;

    std::string line;
    std::getline(input, line);
    arrival = std::stoll(line);
    while (!input.eof()) {
        std::getline(input, line, ',');
        if (line != "x") {
            auto b = std::stoll(line);
            auto diff = arrival % b;
            auto w = diff ? (b - diff) : (0);

            if (w < wait) {
                wait = w;
                bus = b;
            }
        }
    }
    std::cout << bus*wait << std::endl;
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-13.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    // test cases:
    //std::vector<int64_t> busses = {17,0,13,19}; // first occurs at timestamp 3417.
    //std::vector<int64_t> busses = {67,7,59,61}; // first occurs at timestamp 754018.
    //std::vector<int64_t> busses = {67,0,7,59,61}; // first occurs at timestamp 779210.
    //std::vector<int64_t> busses = {67,7,0,59,61}; // first occurs at timestamp 1261476.
    //std::vector<int64_t> busses = {1789,37,47,1889}; // first occurs at timestamp 1202161486.

    std::vector<int64_t> busses;
    {
        std::string line;
        std::getline(input, line); // discard
        while (!input.eof()) {
            std::getline(input, line, ',');
            busses.push_back(line == "x" ? 0 : std::stoll(line));
        }
    }

    auto test = [&](int64_t time, size_t last_bus) {
        for (auto i = 0; i <= last_bus; ++i) {
            if (auto b = busses[i]; b && (time+i)%b != 0) {
                return false;
            }
        }
        return true;
    };

    // for "2,x,3,5":
    // 2: 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38 40 42 44 46 48 50 52 54 56 58 ...
    // 3: 3 6 9 12 15 18 21 24 27 30 33 36 39 42 45 48 51 54 57 60 63 66 69 72 75 78 81 84 87 ...
    // 5: 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75 80 85 90 ...
    // for (2,x,3): 4,6;10,12;16,18;22,24;28,30;...  -> always +6!  (2*3)
    // for (2,x,3,5): 22,24,25;52,54,55;82,84,85;... -> always +30! (2*3*5)
    //
    // These busses happened to be prime, I also tried with a non prime example:
    // the jumps are always the smallest common multiple of all busses!
    // -> we just need to find the start position

    // find smallest common multiple
    auto bus_kgv = [&](size_t from, size_t to) {
        int64_t kgv = 1;
        for (size_t i = from; i <= to; ++i) {
            if (auto b = busses[i]) {
                kgv *= b; // assume they're all prime for now
            }
        }
        return kgv;
    };

    size_t range_end = 1;

    int64_t start_time = 0;
    int64_t jump = busses[0];

    while (range_end < busses.size()) {
        if (busses[range_end]) {
            while (!test(start_time, range_end)) {
                start_time += jump;
            }
            jump = bus_kgv(0, range_end);
        }
        ++range_end;
    }

    std::cout << start_time << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}