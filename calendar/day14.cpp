#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

void puzzle_a()
{
    const std::regex reg_mask("mask = ([01X]+)");
    const std::regex reg_mem("mem\\[([0-9]+)\\] = ([0-9]+)");

    std::smatch matches;

    std::fstream input;
    input.open("../input/input-14.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::map<uint64_t, uint64_t> memory;
    uint64_t hi_mask = 0xfffffffff; // OR
    uint64_t lo_mask = 0x0; // AND

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (std::regex_match(line, matches, reg_mem)) {
            auto addr = std::stoull(matches[1].str());
            auto val = std::stoull(matches[2].str());
            memory[addr] = (val | hi_mask) & lo_mask;
        } else if (std::regex_match(line, matches, reg_mask)) {
            auto mask = matches[1].str();
            hi_mask = 0x0;
            lo_mask = 0x0;
            for (auto c = 0; c < mask.size(); ++c) {
                hi_mask <<= 1;
                lo_mask <<= 1;
                if (mask[c] == 'X') {
                    hi_mask |= 0x0;
                    lo_mask |= 0x1;
                } else if (mask[c] == '1') {
                    hi_mask |= 0x1;
                    lo_mask |= 0x1;
                } else if (mask[c] == '0') {
                    hi_mask |= 0x0;
                    lo_mask |= 0x0;
                }
            }
        }
    }
    int64_t sum = 0;
    for (auto[addr,val] : memory) {
        sum += val;
    }
    std::cout << sum << std::endl;
}

void puzzle_b()
{
    const std::regex reg_mask("mask = ([01X]+)");
    const std::regex reg_mem("mem\\[([0-9]+)\\] = ([0-9]+)");

    std::smatch matches;

    std::fstream input;
    input.open("../input/input-14.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");

    std::map<uint64_t, uint64_t> memory;
    uint64_t hi_mask = 0xfffffffff; // OR
    std::vector<uint64_t> x_mask;

    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
        if (std::regex_match(line, matches, reg_mem)) {
            uint64_t addr = std::stoull(matches[1].str()) | hi_mask;
            uint64_t val = std::stoull(matches[2].str());
            if (x_mask.empty()) {
                memory[addr] = val;
            } else {
                for (uint64_t x = 0; x < (1ull << x_mask.size()); ++x) {
                    for (uint64_t p = 0; p < x_mask.size(); ++p) {
                        if (x & (1 << p)) {
                            addr |= 1ull << x_mask[p];
                        } else {
                            addr &= ~(1ull << x_mask[p]);
                        }
                    }
                    memory[addr] = val;
                }
            }
        } else if (std::regex_match(line, matches, reg_mask)) {
            auto mask = matches[1].str();
            hi_mask = 0x0;
            x_mask.clear();
            for (auto c = 0; c < mask.size(); ++c) {
                hi_mask <<= 1ull;
                if (mask[c] == 'X') {
                    x_mask.push_back(mask.size() - c - 1);
                } else if (mask[c] == '1') {
                    hi_mask |= 0x1;
                } else if (mask[c] == '0') {
                    hi_mask |= 0x0;
                }
            }
        }
    }
    uint64_t sum = 0;
    for (auto[addr,val] : memory) {
        sum += val;
    }
    std::cout << sum << std::endl;
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}