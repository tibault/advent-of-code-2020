#include <iostream>
#include <fstream>
#include <vector>
#include <regex>
#include <set>
#include <sstream>
#include <map>

void puzzle_a()
{
    std::fstream input;
    input.open("../input/input-17.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");
    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
    }
}

void puzzle_b()
{
    std::fstream input;
    input.open("../input/input-17.txt", std::fstream::in);
    if (!input.is_open())
        throw std::runtime_error("Could not open input");
    std::string line;
    while (!input.fail()) {
        std::getline(input, line);
    }
}

int main()
{
    try {
        puzzle_a();
        puzzle_b();

    } catch (const std::exception& ex) {
        std::cerr << "caught error: " << ex.what() << std::endl;
    }
}